
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Нахождение корней квадратных уравнений</title>
      <script type="text/javascript" src="js/jquery-3.1.1.js"></script>
      <style>
          .white {background-color: white; }
          .red-border {border:1pt solid red; }
          .black-border {border:1pt solid black; }
          .gray  {background-color: lightgray; }
          .text-align-center{text-align: center;  }
          .blue-rectangle{
              -webkit-border-radius: 20px;
              -moz-border-radius: 20px;
              border-radius: 20px;
              margin: auto;
              background-color: #add8e6;
              padding: 0px 20px;
              width:800px}
          .result {}
      </style>
  </head>
  <body>
    <script type="text/javascript">
       $( function () {
            $("#queryForm").submit(function() {
                var a = $("#a").val();
                var b = $("#b").val();
                var c = $("#c").val();
                var expr = new RegExp(/^[\+-]?\d+[\.\/]?\d*$/);
                if (expr.test(a) && expr.test(b) && expr.test(c)) {
                    $.ajax({url: '/roots',
                            data: $("#queryForm").serialize(),
                            type: 'POST',
                            success: function(responce){
                        $("<tr class=\"white\"><td class=\"result\">"+$("#a").val() + "x^2 + " +
                            $("#b").val() + "x + " + $("#c").val()+ " = 0</td><td class=\"text-align-center result\">"
                            + responce + "</td></tr>").insertAfter($("#resultsTable").find("tr:first"));
                    }});
                    $("tr").slice(1).filter(":even").each(function () { //Zebra
                        if ($(this).hasClass("white")) $(this).removeClass("white");
                        $(this).addClass("gray");
                    });
                    $("tr").slice(1).filter(":odd").each(function () {
                        if ($(this).hasClass("gray")) $(this).removeClass("gray");
                        $(this).addClass("white");
                    });
                    $("input").each(function () {
                        if($(this).hasClass("red-border")) {
                            $(this).removeClass("red-border");
                            $(this).addClass("black-border");
                        }
                    });
                    $("#resultsTable").on("click",".result",function () {
                        $(this).closest("tr").remove();
                        $("tr").slice(1).filter(":odd").each(function () { //Zebra
                            if ($(this).hasClass("white")) $(this).removeClass("white");
                            $(this).addClass("gray");
                        });
                        $("tr").slice(1).filter(":even").each(function () {
                            if ($(this).hasClass("gray")) $(this).removeClass("gray");
                            $(this).addClass("white");
                        });
                    });
                } else {
                    $("input").each(function () {
                        if(!expr.test($(this).val())) {
                            if($(this).hasClass("black-border")) {
                                $(this).removeClass("black-border");
                                $(this).addClass("red-border");
                            }
                            } else {
                            if($(this).hasClass("red-border")) {
                                $(this).removeClass("red-border");
                                $(this).addClass("black-border");
                            }
                        }
                    });
                    alert("Коэффициенты должны являться целым числом или обыкновеной дробью");
                }
                return false;
            });
        });

    </script>
    <form id="queryForm">
        <div id="controls" align="center" class="blue-rectangle">
        <p><b>Введите коэффициенты уравнения (целые числа, обыкновенные дроби):</b></p>
        <div align="center">
            <input type="text" name = "a" id = "a"  class="black-border"/> x^2 +
            <input type="text" name = "b" id = "b"  class="black-border"/> x +
            <input type="text" name = "c" id = "c"  class="black-border"/> = 0
        </div>
        <br>
        <input type="submit" value="Решить уравнение" align="center"/>
        </div>
    </form>
    <br>
    <table id="resultsTable" align="center" class="black-border">
    <tr bgcolor="#add8e6"><th width="200">Уравнение</th><th width="600" class="text-align-center">Корни</th></tr>
    </table>
  </body>
</html>

package com.edu.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

public class Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String aString = req.getParameter("a");
        String bString = req.getParameter("b");
        String cString = req.getParameter("c");
        double a = aString.contains("/") ? depletion(aString) : Long.decode(aString);
        double b = bString.contains("/") ? depletion(bString) : Long.decode(bString);
        double c = cString.contains("/") ? depletion(cString) : Long.decode(cString);
        double discriminant = Math.pow(b,2) - 4 * a * c;
        Writer writer = resp.getWriter();
        if (a == 0d){
            writer.write(b==0 ? "No roots" : "X: " + (-c)/b);
        } else
        if(discriminant > 0) {
            writer.write("X1: " + (-b-Math.sqrt(discriminant))/(2*a) + ", X2: " + (-b+Math.sqrt(discriminant))/(2*a));
        } else
        if(discriminant == 0 ) {
            writer.write("X: " + (-b)/(2*a));
        } else
        if(discriminant < 0) {
            writer.write("X1: " + (-b)/(2*a) + " - i" + Math.sqrt(Math.abs(discriminant))/(2*a) +
                    ", X2: " + (-b)/(2*a) + " + i" + Math.sqrt(Math.abs(discriminant))/(2*a));
        }
        writer.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    private double depletion (String input) {
        int i = input.indexOf('/');
        double a = i==1 ? Character.getNumericValue(input.charAt(0)) : Long.decode(input.substring(0,i-1));
        double b = i+2==input.length()? Character.getNumericValue(input.charAt(i+1)) : Long.decode(input.substring(i+1));
        return a/b;
    }
}
